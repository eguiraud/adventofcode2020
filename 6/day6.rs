use std::collections::HashSet;
use std::iter::FromIterator;

fn main() {
    let input: Vec<_> = include_str!("input.txt").trim().split("\n\n").collect();

    let part1: usize = input.iter().map(n_unique_chars).sum();
    println!("part 1: {}", part1);

    let part2: usize = input.iter().map(n_chars_on_all_lines).sum();
    println!("part 2: {}", part2);
}

fn n_unique_chars(group_answers: &&str) -> usize {
    group_answers
        .split_whitespace()
        .map(|s| s.chars())
        .flatten()
        .fold(HashSet::new(), |mut acc, c| {
            acc.insert(c);
            acc
        })
        .len()
}

fn n_chars_on_all_lines(group_answers: &&str) -> usize {
    let sets: Vec<HashSet<char>> = group_answers
        .split_whitespace()
        .map(|s| HashSet::from_iter(s.chars()))
        .collect();

    let mut intersection: HashSet<_> = sets[0].clone();
    for set in sets.iter().skip(1) {
        intersection = intersection.intersection(set).copied().collect();
    }
    intersection.len()
}
