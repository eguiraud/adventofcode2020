fn count_trees_in_slope(skip_down: usize, skip_right: usize) -> usize {
    let input = include_str!("input.txt");
    let cols = input.split('\n').next().unwrap().len();
    let rows = input.trim().split('\n').count();
    let grid: Vec<char> = input.chars().filter(|&c| c != '\n').collect();
    let mut current_col = 0;
    let mut n_trees = 0;
    for r in (0..rows).step_by(skip_down) {
        if grid[r * cols + current_col] == '#' {
            n_trees += 1;
        }
        current_col = (current_col + skip_right) % cols;
    }
    n_trees
}

fn main() {
    println!("{}", count_trees_in_slope(1, 3));
    println!(
        "{}",
        count_trees_in_slope(1, 1)
            * count_trees_in_slope(1, 3)
            * count_trees_in_slope(1, 5)
            * count_trees_in_slope(1, 7)
            * count_trees_in_slope(2, 1)
    );
}
