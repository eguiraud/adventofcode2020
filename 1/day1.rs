use std::fs::read_to_string;

fn read_numbers(fname: &str) -> Vec<u32> {
    let contents = read_to_string(fname).unwrap();
    return contents
        .trim()
        .split('\n')
        .map(|s| s.parse().unwrap())
        .collect();
}

fn first_puzzle(numbers: &Vec<u32>) -> u32 {
    for i in 0..numbers.len() {
        for j in (i + 1)..numbers.len() {
            if numbers[i] + numbers[j] == 2020 {
                return numbers[i] * numbers[j];
            }
        }
    }
    panic!("nothing found");
}

fn second_puzzle(numbers: &Vec<u32>) -> u32 {
    for i in 0..numbers.len() {
        for j in (i + 1)..numbers.len() {
            for z in (j + 1)..numbers.len() {
                if numbers[i] + numbers[j] + numbers[z] == 2020 {
                    return numbers[i] * numbers[j] * numbers[z];
                }
            }
        }
    }
    panic!("nothing found");
}

fn main() {
    let numbers = read_numbers("input.txt");
    println!("{}", first_puzzle(&numbers));
    println!("{}", second_puzzle(&numbers));
}
