fn main() {
    let seat_ids = {
        let mut v: Vec<_> = include_str!("input.txt")
            .trim()
            .split('\n')
            .map(seat_id)
            .collect();
        v.sort();
        v
    };

    let highest_seat_id = seat_ids.iter().max().unwrap();
    println!("{}", highest_seat_id);

    let mut last = seat_ids[0] - 1;
    seat_ids.iter().take_while(|&&e| { last += 1; e == last }).last();
    println!("{:?}", last);
}

fn seat_id(seat: &str) -> u32 {
    let row = binary_partition(&seat[..7]);
    let column = binary_partition(&seat[7..]);
    row * 8 + column
}

fn binary_partition(partition: &str) -> u32 {
    let max_pow = partition.len() - 1;
    partition
        .chars()
        .enumerate()
        .map(|(i, c)| 2_u32.pow((max_pow - i) as u32) * high_or_low(c))
        .sum()
}

fn high_or_low(c: char) -> u32 {
    match &c {
        'F' | 'L' => 0,
        'B' | 'R' => 1,
        _ => unreachable!(),
    }
}
