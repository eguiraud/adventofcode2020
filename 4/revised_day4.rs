#[macro_use]
extern crate lazy_static;
use regex::Regex;

fn main() {
    let n_valid_passports: usize = include_str!("input.txt")
        .split("\n\n")
        .map(|passport| {
            passport
                .split_whitespace()
                .map(|token| token.splitn(2, ':'))
                .map(|mut split| {
                    (
                        split.next().unwrap().to_string(),
                        split.next().unwrap().to_string(),
                    )
                })
                .collect()
        })
        .filter(has_valid_fields)
        .count();

    println!("{}", n_valid_passports);
}

fn has_valid_fields(pass: &Vec<(String, String)>) -> bool {
    lazy_static! {
        static ref VALID_HAIR_COLOR: Regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
        static ref VALID_PASS_ID: Regex = Regex::new(r"^\d{9}$").unwrap();
    }

    let n_valid_fields = pass
        .iter()
        .filter(|(k, v)| match k.as_str() {
            "byr" => (1920..=2002).contains(&v.parse().unwrap()),
            "iyr" => (2010..=2020).contains(&v.parse().unwrap()),
            "eyr" => (2020..=2030).contains(&v.parse().unwrap()),
            "ecl" => ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&v.as_str()),
            "hcl" => VALID_HAIR_COLOR.is_match(v),
            "pid" => VALID_PASS_ID.is_match(v),
            "hgt" => {
                let len = v.len();
                match &v[len - 2..] {
                    "cm" => (150..=193).contains(&v[..len - 2].parse().unwrap()),
                    "in" => (59..=76).contains(&v[..len - 2].parse().unwrap()),
                    _ => false,
                }
            }
            _ => false,
        })
        .count();

    n_valid_fields == 7
}
