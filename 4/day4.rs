/*
byr (Birth Year) - four digits; at least 1920 and at most 2002.
iyr (Issue Year) - four digits; at least 2010 and at most 2020.
eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
hgt (Height) - a number followed by either cm or in:
If cm, the number must be at least 150 and at most 193.
If in, the number must be at least 59 and at most 76.
hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
pid (Passport ID) - a nine-digit number, including leading zeroes.
cid (Country ID) - ignored, missing or not.
*/

#[macro_use]
extern crate lazy_static;

use regex::Regex;

fn check_byr(pass: &&str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(^|[[:space:]])byr:(\d\d\d\d)($|[[:space:]])").unwrap();
    }
    let n: Option<u32> = RE
        .captures(pass)
        .map(|c| c.get(2).unwrap().as_str().parse::<u32>().unwrap());
    n.map(|x| x >= 1920 && x <= 2002).unwrap_or(false)
}

fn check_iyr(pass: &&str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(^|[[:space:]])iyr:(\d\d\d\d)([[:space:]]|$)").unwrap();
    }
    let n: Option<u32> = RE
        .captures(pass)
        .map(|c| c.get(2).unwrap().as_str().parse::<u32>().unwrap());
    n.map(|x| x >= 2010 && x <= 2020).unwrap_or(false)
}

fn check_eyr(pass: &&str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(^|[[:space:]])eyr:(\d\d\d\d)($|[[:space:]])").unwrap();
    }
    let n: Option<u32> = RE
        .captures(pass)
        .map(|c| c.get(2).unwrap().as_str().parse::<u32>().unwrap());
    n.map(|x| x >= 2020 && x <= 2030).unwrap_or(false)
}

fn check_hgt(pass: &&str) -> bool {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r"(^|[[:space:]])hgt:(\d\d\d?)(cm|in)($|[[:space:]])").unwrap();
    }
    let height: Option<(u32, &str)> = RE.captures(pass).map(|c| {
        let hgt = c.get(2).unwrap().as_str().parse::<u32>().unwrap();
        let unit = c.get(3).unwrap().as_str();
        (hgt, unit)
    });
    height
        .map(|(hgt, unit)| match unit {
            "cm" => hgt >= 150 && hgt <= 193,
            "in" => hgt >= 59 && hgt <= 76,
            _ => false,
        })
        .unwrap_or(false)
}

fn check_hcl(pass: &&str) -> bool {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r"(^|[[:space:]])hcl:#[0-9a-f]{6}($|[[:space:]])").unwrap();
    }
    RE.is_match(pass)
}

fn check_ecl(pass: &&str) -> bool {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r"(^|[[:space:]])ecl:(amb|blu|brn|gry|grn|hzl|oth)($|[[:space:]])").unwrap();
    }
    RE.is_match(pass)
}

fn check_pid(pass: &&str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(^|[[:space:]])pid:[0-9]{9}($|[[:space:]])").unwrap();
    }
    RE.is_match(pass)
}

fn validate_fields(pass: &&&str) -> bool {
    let checks = [
        check_byr, check_iyr, check_eyr, check_hgt, check_hcl, check_ecl, check_pid,
    ];
    checks.iter().all(|check| check(pass))
}

fn has_all_fields(pass: &&&str) -> bool {
    let mandatory_fields = ["byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"];
    mandatory_fields.iter().all(|s| pass.find(s).is_some())
}

fn main() {
    let passports: Vec<&str> = include_str!("input.txt").split("\n\n").collect();
    println!(
        "part 1: {}",
        passports.iter().filter(has_all_fields).count()
    );
    println!(
        "part 2: {}",
        passports.iter().filter(validate_fields).count()
    );
}
