#[macro_use]
extern crate lazy_static;

use regex::Regex;
use std::fs::read_to_string;

struct Password {
    x: usize,
    y: usize,
    letter: char,
    pw: String,
}

impl Password {
    fn new(pw_str: &str) -> Password {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^(\d{1,})-(\d{1,}) ([a-z]): ([a-z]{1,})$").unwrap();
        }
        let caps = RE.captures(pw_str).unwrap();
        let x: usize = (*caps.get(1).unwrap().as_str()).parse().unwrap();
        let y: usize = (*caps.get(2).unwrap().as_str()).parse().unwrap();
        let letter: char = (*caps.get(3).unwrap().as_str()).parse().unwrap();
        let pw: String = caps.get(4).unwrap().as_str().to_string();
        Password { x, y, letter, pw }
    }
}

fn is_valid_old_policy(pass: &Password) -> bool {
    let letter_counts = pass.pw.chars().filter(|&c| c == pass.letter).count();
    if letter_counts >= pass.x && letter_counts <= pass.y {
        return true;
    }
    false
}

fn first(fname: &str) -> usize {
    read_to_string(fname)
        .unwrap()
        .trim()
        .split('\n')
        .map(|s| Password::new(s))
        .filter(is_valid_old_policy)
        .count()
}

fn is_valid_new_policy(pass: &Password) -> bool {
    let correctly_positioned_letters = (pass.pw.chars().nth(pass.x - 1) == Some(pass.letter))
        as i32
        + (pass.pw.chars().nth(pass.y - 1) == Some(pass.letter)) as i32;
    correctly_positioned_letters == 1
}

fn second(fname: &str) -> usize {
    read_to_string(fname)
        .unwrap()
        .trim()
        .split('\n')
        .map(|s| Password::new(s))
        .filter(is_valid_new_policy)
        .count()
}

fn main() {
    println!("{}", first("input.txt"));
    println!("{}", second("input.txt"));
}
